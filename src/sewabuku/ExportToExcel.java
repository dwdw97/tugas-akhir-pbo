package sewabuku;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JTable;
import javax.swing.table.TableModel;

public class ExportToExcel {
	TableModel  tableModel;
	FileWriter fOut;
	int counter;

	public ExportToExcel(JTable table, File file) {
		counter = 0;
		try {
			tableModel = table.getModel();
			fOut = new FileWriter(file);
			for(int i=0; i<tableModel.getColumnCount(); i++) {
				fOut.write(tableModel.getColumnName(i)+"\t");
			}
			fOut.write("\n");
			cetakDataExcel();
			fOut.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void cetakDataExcel() throws IOException {
		try {
			for(int i=counter; i<tableModel.getRowCount(); i++) {
				for(int j=0; j<tableModel.getColumnCount(); j++) {
					fOut.write(tableModel.getValueAt(i, j).toString()+"\t");
				}
				fOut.write("\n");
				counter++;
			}
		} catch(NullPointerException npe) {
//			npe.printStackTrace();
			fOut.write("\n");
			counter++;
			cetakDataExcel();
		}
	}
}
