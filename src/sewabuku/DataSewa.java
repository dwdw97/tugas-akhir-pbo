package sewabuku;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import database.DatabaseManager;

import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuItem;

public class DataSewa extends JFrame {

	public JFrame formDataSewa;
	private JTextField txtPeminjam;
	public static JTable tableSewa;
	private JComboBox boxJudulBuku;
	private JLabel lblTanggal;
	private JLabel lblJam;
	private JButton btnKembalikanBuku;

	// java-mysql connect
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://127.0.0.1/sewabuku";
	static final String USER = "root";
	static final String PASS = "";
	
	static Connection conn;
	static Statement stmt;
	static ResultSet rs;
	private DatabaseManager db = new DatabaseManager();
	
	private DefaultTableModel model;
	SimpleDateFormat timeFormat;
	SimpleDateFormat dateFormat;
	String time;
	String date;
	Runnable runnable;
	Date currentDate;
	Date tanggalHarusKembali;
	
	private String[] judul_buku;
	private int jmlBuku;
	private int counter;
	private int denda;
	private Map<String, String[]> listBuku = new HashMap<String, String[]>();
		
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					DataSewaFix window = new DataSewaFix();
//					window.formDataSewa.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 */
	public DataSewa() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		formDataSewa = new JFrame();
		formDataSewa.setTitle("Data Sewa");
		formDataSewa.setResizable(false);
		formDataSewa.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				showData();
				showClock();
				showDate();
			}
		});
		formDataSewa.setBounds(100, 100, 582, 470);
		formDataSewa.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		formDataSewa.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Persewaan Buku XYZ");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(10, 11, 546, 14);
		formDataSewa.getContentPane().add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(10, 36, 546, 29);
		formDataSewa.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Tanggal:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_1.setBounds(10, 0, 50, 29);
		panel.add(lblNewLabel_1);
		
		lblTanggal = new JLabel("");
		lblTanggal.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTanggal.setBounds(62, 0, 117, 29);
		panel.add(lblTanggal);
		
		JLabel lblNewLabel_1_1 = new JLabel("Jam:");
		lblNewLabel_1_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1_1.setBounds(451, 0, 30, 29);
		panel.add(lblNewLabel_1_1);
		
		lblJam = new JLabel("");
		lblJam.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblJam.setBounds(480, 0, 56, 29);
		panel.add(lblJam);
		
		JLabel lblNewLabel_2 = new JLabel("Nama Peminjam");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_2.setBounds(32, 94, 75, 14);
		formDataSewa.getContentPane().add(lblNewLabel_2);
		
		txtPeminjam = new JTextField();
		txtPeminjam.setBounds(117, 91, 120, 20);
		formDataSewa.getContentPane().add(txtPeminjam);
		txtPeminjam.setColumns(10);
		
		JLabel lblNewLabel_2_1 = new JLabel("Judul Buku");
		lblNewLabel_2_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_2_1.setBounds(32, 122, 75, 14);
		formDataSewa.getContentPane().add(lblNewLabel_2_1);
		
		JLabel lblNewLabel_3 = new JLabel("Biaya      Rp");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_3.setBounds(258, 106, 70, 14);
		formDataSewa.getContentPane().add(lblNewLabel_3);
		
		JLabel lblBiaya = new JLabel("");
		lblBiaya.setHorizontalAlignment(SwingConstants.LEFT);
		lblBiaya.setVerticalAlignment(SwingConstants.BOTTOM);
		lblBiaya.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblBiaya.setBounds(328, 97, 120, 27);
		formDataSewa.getContentPane().add(lblBiaya);
		
		JButton btnSimpan = new JButton("Simpan");
		btnSimpan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nama = txtPeminjam.getText();
				String judul = boxJudulBuku.getSelectedItem().toString();
				int buku_id = Integer.parseInt(listBuku.get(judul)[0]);
				if(listBuku.get(judul)[2].equals("0")) {
					JOptionPane.showMessageDialog(null, "Maaf, buku "+judul+" sedang tidak tersedia");
				} else {
					simpanData(nama, buku_id);
					ubahStatusBuku(buku_id, 0);
					listBuku.get(judul)[2] = "0";
				}
			}
		});
		btnSimpan.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnSimpan.setBounds(10, 163, 89, 23);
		formDataSewa.getContentPane().add(btnSimpan);
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(tableSewa.getValueAt(tableSewa.getSelectedRow(), 0).toString());
				String judul_lama = tableSewa.getValueAt(tableSewa.getSelectedRow(), 2).toString();
				String judul_baru = boxJudulBuku.getSelectedItem().toString();
				int buku_id_lama = Integer.parseInt(listBuku.get(judul_lama)[0]);
				int buku_id_baru = Integer.parseInt(listBuku.get(judul_baru)[0]);
				if(listBuku.get(judul_baru)[2].equals("0")) {
					JOptionPane.showMessageDialog(null, "Maaf, buku "+judul_baru+" sedang tidak tersedia");
				} else {
					ubahData(id, buku_id_baru);
					ubahStatusBuku(buku_id_baru, 0);
					listBuku.get(judul_baru)[2] = "0";
					ubahStatusBuku(buku_id_lama, 1);
					listBuku.get(judul_lama)[2] = "1";
					
					txtPeminjam.setText("");
					txtPeminjam.setEnabled(true);
					boxJudulBuku.setSelectedIndex(0);
					boxJudulBuku.setEnabled(true);
					btnSimpan.setEnabled(true);
					btnEdit.setEnabled(false);
					btnKembalikanBuku.setEnabled(false);
					lblBiaya.setText("");
					tableSewa.clearSelection();
				}
			}
		});
		btnEdit.setEnabled(false);
		btnEdit.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnEdit.setBounds(109, 163, 61, 23);
		formDataSewa.getContentPane().add(btnEdit);
		
		btnKembalikanBuku = new JButton("Kembalikan Buku");
		btnKembalikanBuku.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(tableSewa.getValueAt(tableSewa.getSelectedRow(), 0).toString());
				String judul = tableSewa.getValueAt(tableSewa.getSelectedRow(), 2).toString();
				int buku_id = Integer.parseInt(listBuku.get(judul)[0]);
				kembalikanBuku(id);
				ubahStatusBuku(buku_id, 1);
				listBuku.get(buku_id)[2] = "1";
			}
		});
		btnKembalikanBuku.setEnabled(false);
		btnKembalikanBuku.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnKembalikanBuku.setBounds(258, 163, 120, 23);
		formDataSewa.getContentPane().add(btnKembalikanBuku);
		
		JButton btnCetakLaporan = new JButton("Cetak Laporan");
		btnCetakLaporan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Reports buku_report = new Reports("sewabuku");
			}
		});
		btnCetakLaporan.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnCetakLaporan.setBounds(442, 163, 114, 23);
		formDataSewa.getContentPane().add(btnCetakLaporan);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 197, 546, 201);
		formDataSewa.getContentPane().add(scrollPane);
		
		tableSewa = new JTable();
		tableSewa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int id = Integer.parseInt(tableSewa.getValueAt(tableSewa.getSelectedRow(), 0).toString());
				String judul = tableSewa.getValueAt(tableSewa.getSelectedRow(), 2).toString();
				getData(id, judul);
				btnSimpan.setEnabled(false);
				try {
					String tglKembali = tableSewa.getValueAt(tableSewa.getSelectedRow(), 4).toString();
					if(tglKembali.isEmpty()) {
						btnEdit.setEnabled(true);
						btnKembalikanBuku.setEnabled(true);
						boxJudulBuku.setEnabled(true);
						lblBiaya.setText("");
					} else {
						int biaya = Integer.parseInt(tableSewa.getValueAt(tableSewa.getSelectedRow(), 6).toString());
						btnEdit.setEnabled(false);
						btnKembalikanBuku.setEnabled(false);
						boxJudulBuku.setEnabled(false);
						lblBiaya.setText(currencyFormatter(biaya));
					}
				} catch(NullPointerException npe) {
					btnEdit.setEnabled(true);
					btnKembalikanBuku.setEnabled(true);
					boxJudulBuku.setEnabled(true);
					lblBiaya.setText("");
				}
				txtPeminjam.setEnabled(false);
			}
		});
		scrollPane.setViewportView(tableSewa);
		
		boxJudulBuku = new JComboBox();
		boxJudulBuku.setFont(new Font("Tahoma", Font.PLAIN, 11));
		boxJudulBuku.setBounds(117, 118, 120, 22);
		formDataSewa.getContentPane().add(boxJudulBuku);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtPeminjam.setText("");
				txtPeminjam.setEnabled(true);
				boxJudulBuku.setSelectedIndex(0);
				boxJudulBuku.setEnabled(true);
				btnSimpan.setEnabled(true);
				btnEdit.setEnabled(false);
				btnKembalikanBuku.setEnabled(false);
				lblBiaya.setText("");
				tableSewa.clearSelection();
			}
		});
		btnReset.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnReset.setBounds(180, 163, 66, 23);
		formDataSewa.getContentPane().add(btnReset);
		
		JMenuBar menuBarSewa = new JMenuBar();
		formDataSewa.setJMenuBar(menuBarSewa);
		
		JMenu menuDataBuku = new JMenu("Menu");
		menuDataBuku.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		menuBarSewa.add(menuDataBuku);
		
		JMenuItem mntmDataBuku = new JMenuItem("Data Buku");
		mntmDataBuku.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mntmDataBuku.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DataBuku dataBuku = new DataBuku();
				dataBuku.formDataBuku.setVisible(true);
				formDataSewa.dispose();
			}
		});
		menuDataBuku.add(mntmDataBuku);
	}
	
	public void showClock() {
		timeFormat = new SimpleDateFormat("HH:mm:ss");
		
		runnable = new Runnable() {
	        @Override
	        public void run() {
	          while (true) {
	            currentDate = getDate();
	            time = timeFormat.format(currentDate);
	            lblJam.setText(time);
	            try {
	              Thread.sleep(1000);
	            }
	            catch (InterruptedException e) {
	              e.printStackTrace();
	            }
	          }
	        }
	    };
	    Thread t = new Thread(runnable);
	    t.start();
	}
	
	public void showDate() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		currentDate = getDate();
		date = dateFormat.format(currentDate);
		lblTanggal.setText(date);
	}
	
	public static java.util.Date getDate() {
		java.util.Date date = new java.util.Date();
		return date;
    }
	
	public void getJudulBuku() {		
		try {
			conn = db.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery("SELECT buku_id, judul_buku, status FROM buku");
			while(rs.next()) {
				listBuku.put(rs.getString("judul_buku"), new String[3]);
				listBuku.get(rs.getString("judul_buku"))[0] = rs.getString("buku_id");
				listBuku.get(rs.getString("judul_buku"))[1] = rs.getString("judul_buku");
				listBuku.get(rs.getString("judul_buku"))[2] = rs.getString("status");
			}
			
			stmt.close();
			conn.close();
			
			judul_buku = new String[listBuku.size()];
			counter = listBuku.size()-1;
			listBuku.forEach((judul,value)->{
				judul_buku[counter] = value[1];
				counter--;
			});
			
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
	}
	
	public void showData() {
		model = new DefaultTableModel();
		
		model.addColumn("No");
		model.addColumn("Peminjam");
		model.addColumn("Judul Buku");
		model.addColumn("Tanggal Pinjam");
		model.addColumn("Tanggal Dikembalikan");
		model.addColumn("Denda");
		model.addColumn("Biaya Total");
		
		try {
			conn = db.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery("SELECT id, nama_peminjam, buku.judul_buku, date_format(tanggal_pinjam, '%d-%m-%Y'), date_format(tanggal_kembali, '%d-%m-%Y'), denda, biaya_sewa, buku.buku_id, buku.status FROM `sewabuku` INNER JOIN buku ON sewabuku.`buku_id`=buku.buku_id");
			while(rs.next()) {
				model.addRow(new Object[] {
					rs.getString("id"),
					rs.getString("nama_peminjam"),
					rs.getString("judul_buku"),
					rs.getString("date_format(tanggal_pinjam, '%d-%m-%Y')"),
					rs.getString("date_format(tanggal_kembali, '%d-%m-%Y')"),
					rs.getString("denda"),
					rs.getString("biaya_sewa")
				});
			}
			
			stmt.close();
			conn.close();
			
			tableSewa.setModel(model);
			tableSewa.setAutoResizeMode(1);
			
			getJudulBuku();
			boxJudulBuku.setModel(new DefaultComboBoxModel(judul_buku));
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
	}
	
	public void getData(int buku_id, String judul) {
		try {
			conn = db.getConnection();
			
			String query = "SELECT * FROM sewabuku WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setInt(1, buku_id);
			
			rs = ps.executeQuery();
			
			rs.next();
			
			txtPeminjam.setText(rs.getString("nama_peminjam"));
			boxJudulBuku.setSelectedItem(judul);
			
			stmt.close();
			conn.close();
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
	}
	
	public void simpanData(String nama_peminjam, int buku_id) {
		try {
			conn = db.getConnection();
			
			String query = "INSERT INTO sewabuku (nama_peminjam, buku_id, tanggal_pinjam, tanggal_harus_kembali) VALUES (?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setString(1, nama_peminjam);
			ps.setInt(2, buku_id);
			ps.setDate(3, new java.sql.Date(getDate().getTime()));
			ps.setDate(4, new java.sql.Date(getDate().getTime()+TimeUnit.DAYS.toMillis(7)));
			// 604800000 is 7 day in milliseconds
			
			ps.execute();
			
			stmt.close();
			conn.close();			
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
		
		showData();
	}
	
	public void ubahData(int id, int buku_id) {
		try {
			conn = db.getConnection();
			
			String query = "UPDATE sewabuku SET buku_id = ? WHERE id=?";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setInt(1, buku_id);
			ps.setInt(2, id);
			
			ps.execute();
			
			stmt.close();
			conn.close();
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
		
		showData();
	}
	
	public void kembalikanBuku(int id) {
		getTanggalHarusKembali(id);
		if(tanggalHarusKembali.getTime() < getDate().getTime()) {
			long selisihHari = (getDate().getTime() - tanggalHarusKembali.getTime()) / 86400000;
			denda = Integer.parseInt(Long.toString(selisihHari)) * 2000;
		} else {
			denda = 0;
		}
		
		try {
			conn = db.getConnection();
			
			String query = "UPDATE sewabuku SET tanggal_kembali = ?, denda = ?, biaya_sewa = ? WHERE id=?";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setDate(1, new java.sql.Date(getDate().getTime()));
			ps.setInt(2, denda);
			ps.setInt(3, denda+5000);
			ps.setInt(4, id);
			
			ps.execute();
			
			stmt.close();
			conn.close();			
			
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
		
		showData();
	}
	
	public void getTanggalHarusKembali(int id) {
		try {
			conn = db.getConnection();
			
			String query = "SELECT tanggal_harus_kembali FROM sewabuku WHERE id=?";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setInt(1, id);
			
			rs = ps.executeQuery();
			
			rs.next();
			
			java.sql.Date sqlDate = rs.getDate("tanggal_harus_kembali");
			tanggalHarusKembali = new java.util.Date(sqlDate.getTime());
			
			stmt.close();
			conn.close();			
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
	}
	
	public void ubahStatusBuku(int buku_id, int newStatus) {
		try {
			conn = db.getConnection();
			
			String query = "UPDATE buku SET status = ? WHERE buku_id=?";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setInt(1, newStatus);
			ps.setInt(2, buku_id);
			
			ps.execute();
			
			stmt.close();
			conn.close();			
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
	}
	
	private String currencyFormatter(int currency) {
		String pattern = "###,###.00";
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator(',');
		symbols.setGroupingSeparator('.');
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		
		return decimalFormat.format(currency);
	}
}
