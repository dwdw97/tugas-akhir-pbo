package sewabuku;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class Main {

	private JFrame formMain;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.formMain.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		formMain = new JFrame();
		formMain.setResizable(false);
		formMain.setBounds(100, 100, 244, 131);
		formMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBarMain = new JMenuBar();
		formMain.setJMenuBar(menuBarMain);
		
		JMenu mnNewMenu = new JMenu("Menu");
		mnNewMenu.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		menuBarMain.add(mnNewMenu);
		
		JMenuItem mntmDataBuku = new JMenuItem("Data Buku");
		mntmDataBuku.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mntmDataBuku.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DataBuku dataBuku = new DataBuku();
				dataBuku.formDataBuku.setVisible(true);
				formMain.dispose();
			}
		});
		mnNewMenu.add(mntmDataBuku);
		
		JMenuItem mntmDataSewa = new JMenuItem("Data Sewa");
		mntmDataSewa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DataSewa dataSewa = new DataSewa();
				dataSewa.formDataSewa.setVisible(true);
				formMain.dispose();
			}
		});
		mntmDataSewa.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mnNewMenu.add(mntmDataSewa);
	}

}
